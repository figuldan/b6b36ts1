package cz.fel.cvut.ts1;

import cz.cvut.fel.ts1.hw2.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    Calculator calculator;
    public CalculatorTest() {
        this.calculator = new Calculator();
    }

    @Test
    public void sumTest() {
        assertEquals(15, calculator.sum(10, 5));
        assertEquals(25, calculator.sum(15, 10));
        assertEquals(25, calculator.sum(10, 15));
        assertEquals(-10, calculator.sum(-20, 10));
        assertEquals(0, calculator.sum(-25, 25));
    }

    @Test
    public void subtractionTest() {
        assertEquals(15, calculator.subtract(20, 5));
        assertEquals(25, calculator.subtract(50, 25));
        assertEquals(-25, calculator.subtract(25, 50));
        assertEquals(10, calculator.subtract(-20, -30));
        assertEquals(50, calculator.subtract(25, -25));
    }
    @Test
    public void multiplicationTest() {
        assertEquals(50, calculator.multiply(10, 5));
        assertEquals(100, calculator.multiply(2, 50));
        assertEquals(0, calculator.multiply(0, 20));
        assertEquals(-0, calculator.multiply(-20, 0));
        assertEquals(100, calculator.multiply(10, 10));
    }

    @Test
    public void divisionTest() {
        assertEquals(2, calculator.divide(10, 5));
        assertEquals(10, calculator.divide(100, 10));
        assertEquals(3, calculator.divide(75, 25));
        assertEquals(-2, calculator.divide(-20, 10));
        assertThrows(ArithmeticException.class, () -> calculator.divide(10, 0));
        assertThrows(ArithmeticException.class, () -> calculator.divide(-10, 0));
    }


}

