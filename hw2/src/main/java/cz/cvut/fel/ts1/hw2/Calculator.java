package cz.cvut.fel.ts1.hw2;

public class Calculator {

    public long sum(long a, long b) {
        return a + b;
    }

    public long subtract(long a, long b) {
        return a - b;
    }

    public long multiply (long a, long b) {
        return a * b;
    }

    public long divide(long a, long b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException();
        }
        return a / b;
    }
}
